package com.cis.joystick;

import com.jme3.app.SimpleApplication;
import com.jme3.input.event.TouchEvent;
import com.jme3.input.event.TouchEvent.Type;
import static com.jme3.math.FastMath.abs;
import com.jme3.math.Vector2f;
import com.jme3.ui.Picture;

/**
 *
 * @author jo
 */
public class Joystick {
    private final SimpleApplication application;
    private final Vector2f positionBackground;
    private Vector2f positionHandle;
    private final Vector2f dimensionBackground;
    private final String backgroundImg;
    private final String handleImg;
    private final float overhangs;//how much around the joystick background area is treated as a joystick touch too
    private final float minRatio;
    private boolean rememberAfterUp = false;
    private float nPoint, wPoint, ePoint, sPoint;
    private int joystickPointerId;
    private boolean joystickDown;
    private float tmpX, tmpY;
    private final Vector2f tmpTouchEvent = new Vector2f();
    private Vector2f tmpTranslationFromCenter = new Vector2f();
    private Vector2f tmpTranslatedHandlePosition = new Vector2f();
    private Vector2f fromPositionToMiddle = new Vector2f();
    private Vector2f positionCenterHandle = new Vector2f();
    private Picture handlePicture;
    private Picture backPicture;
    private float halfSize;
    
    private Vector2f lastVectorReturned;

    //currently expects handle size to be half background size
    //overhangs : area around the picture who's value will still be read
    //minRatio : ratio between translation and max translation under what a ZERO vector is returned
    public Joystick(SimpleApplication application, Vector2f positionBackground,
            Vector2f dimensionBackground, String backgroundImg, String handleImg, float overhangs, float minRatio, boolean rememberAfterUp) {
        this.application = application;
        this.positionBackground = positionBackground;
        this.dimensionBackground = dimensionBackground;
        this.backgroundImg = backgroundImg;
        this.handleImg = handleImg;
        this.overhangs = overhangs;
        this.minRatio = minRatio;
        this.rememberAfterUp = rememberAfterUp;
    }

    public void initialize() {
        //calculate the cardinal extremities
        nPoint = positionBackground.y + dimensionBackground.x + overhangs;
        sPoint = positionBackground.y - overhangs;
        wPoint = positionBackground.x - overhangs;
        ePoint = positionBackground.x + dimensionBackground.x + overhangs;

        fromPositionToMiddle = dimensionBackground.mult(0.25f);
        positionHandle = positionBackground.add(fromPositionToMiddle);
        positionCenterHandle = positionHandle.add(fromPositionToMiddle);

        halfSize = dimensionBackground.x / 2f;

        joystickPointerId = -1;
        joystickDown = false;
        lastVectorReturned = Vector2f.ZERO;
    }

    private void display() {
        backPicture = new Picture("backPic");
        backPicture.setImage(application.getAssetManager(), backgroundImg, true);
        backPicture.setWidth(dimensionBackground.getX());
        backPicture.setHeight(dimensionBackground.getY());
        backPicture.setPosition(positionBackground.x, positionBackground.y);
//        backPicture.setUserData(GeometryBatchFactoryUtil.FLAG_FOR_OPTIMIZE, "stuff");//todo, a setter
        application.getGuiNode().attachChild(backPicture);

        handlePicture = new Picture("handlePic");
        handlePicture.setImage(application.getAssetManager(), handleImg, true);
        handlePicture.setWidth(dimensionBackground.getX() / 2f);
        handlePicture.setHeight(dimensionBackground.getY() / 2f);
        handlePicture.setPosition(positionHandle.x, positionHandle.y);
        application.getGuiNode().attachChild(handlePicture);
    }

    //returns false when not a joystick event
    //returns Vector2f.ZERO when jotsick in rest position (middle)
    //sets the lastVectorValue when should otherwise keeps previous value
    public boolean lookIntoIt(TouchEvent touchEvent) {
        if (!joystickDown) {
            if (touchEvent.getType() == Type.DOWN) {
                if (touchEvent.getY() < nPoint && touchEvent.getY() > sPoint && touchEvent.getX() > wPoint && touchEvent.getX() < ePoint) {
                    tmpTouchEvent.set(touchEvent.getX(), touchEvent.getY());
                    tmpTranslationFromCenter = tmpTouchEvent.subtract(positionCenterHandle);
                    tmpTranslatedHandlePosition = positionHandle.add(tmpTranslationFromCenter);

                    handlePicture.setPosition(tmpTranslatedHandlePosition.x, tmpTranslatedHandlePosition.y);
                    joystickPointerId = touchEvent.getPointerId();
                    joystickDown = true;

                    tmpTranslationFromCenter.x = tmpTranslationFromCenter.x / halfSize;
                    tmpTranslationFromCenter.y = tmpTranslationFromCenter.y / halfSize;

                    return applyMinRatioAndSetValues(tmpTranslationFromCenter);
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        if (touchEvent.getPointerId() != joystickPointerId) {
            return false;
        }
        
        switch (touchEvent.getType()) {
            case UP:
                if (rememberAfterUp) {
                    //same as MOVE - todo
                    lastVectorReturned = Vector2f.ZERO;
                    return true;
                    
                } else {
                    handlePicture.setPosition(positionHandle.x, positionHandle.y);
                    joystickDown = false;
                    joystickPointerId = - 1;
                    lastVectorReturned = Vector2f.ZERO;
                    return true;
                }
            case MOVE:
                tmpX = (touchEvent.getX() < wPoint) ? wPoint : touchEvent.getX();
                tmpX = (tmpX > ePoint) ? ePoint : tmpX;
                tmpY = (touchEvent.getY() > nPoint) ? nPoint : touchEvent.getY();
                tmpY = (tmpY < sPoint) ? sPoint : tmpY;
                tmpTouchEvent.set(tmpX, tmpY);

                tmpTranslationFromCenter = tmpTouchEvent.subtract(positionCenterHandle);
                tmpTranslatedHandlePosition = positionHandle.add(tmpTranslationFromCenter);

                handlePicture.setPosition(tmpTranslatedHandlePosition.x, tmpTranslatedHandlePosition.y);

                tmpTranslationFromCenter.x = tmpTranslationFromCenter.x / halfSize;
                tmpTranslationFromCenter.y = tmpTranslationFromCenter.y / halfSize;

                return applyMinRatioAndSetValues(tmpTranslationFromCenter);
        }
        return true;
    }

    private boolean applyMinRatioAndSetValues(Vector2f value) {
        lastVectorReturned = value;
        if (minRatio != 0) {
            if( abs(lastVectorReturned.x ) < minRatio) { lastVectorReturned.x = 0; }
            if( abs(lastVectorReturned.y ) < minRatio) { lastVectorReturned.y = 0; }
            if( value.x == 0f && value.y == 0f ){ lastVectorReturned = Vector2f.ZERO; }
        }
        return true;
    }

    public Vector2f getLastVectorValue(){
        return lastVectorReturned;
    }
    
    public void enable() {
        display();
    }

    public void disable() {
    }

    public void clean() {
        application.getGuiNode().detachChild(backPicture);
        application.getGuiNode().detachChild(handlePicture);
    }

    public void update(float tpf) {
    }
}
