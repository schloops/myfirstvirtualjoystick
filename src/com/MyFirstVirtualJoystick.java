package com;

import com.jme3.app.SimpleApplication;
import com.jme3.renderer.RenderManager;

/**
 * test
 * @author jo
 */
public class MyFirstVirtualJoystick extends SimpleApplication {

    public static void main(String[] args) {
        MyFirstVirtualJoystick app = new MyFirstVirtualJoystick();
        app.start();
    }

    @Override
    public void simpleInitApp() {
        System.out.println("MyFirstVirtualJoystick launches ok");
    }

    @Override
    public void simpleUpdate(float tpf) {
    }

    @Override
    public void simpleRender(RenderManager rm) {
    }
}
